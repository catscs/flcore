// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FLCore",
    products: [
        .library(name: "FLWebService", targets: ["FLWebService"]),
        .library(name: "FLImageDownload", targets: ["FLImageDownload"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        .target(name: "FLWebService", dependencies: []),
        .target(name: "FLImageDownload", dependencies: ["FLWebService"])
    ]
)
