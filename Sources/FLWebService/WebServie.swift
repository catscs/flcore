//
//  WebService.swift
//  FLCore
//
//  Created by flujan on 30/7/22.
//

import Foundation
import UIKit

public struct WebService {
    public typealias ResponseCompletionHanlder = (_ reponse: Response) -> Void
}

extension WebService {
    
    public enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
        case patch = "PATCH"
    }
}

extension WebService {
     
    public enum ContentType: String {
        case raw = "application/json"
        case fromData = "multipart/form-data"
        case urlEncode = "application/x-www-form-urlencoded"
    }
}

extension WebService {
    
    public struct Configuration {
        
        fileprivate let timeout: TimeInterval
        fileprivate var httpAditionalsHeaders: [String: Any]
        fileprivate let contentType: ContentType
        fileprivate let isJson: Bool
        
        public init(headers: [String: Any] = [:], contentType: ContentType = .raw, timeout: TimeInterval = 60, isJson: Bool = true) {
            self.contentType = contentType
            self.timeout = timeout
            self.httpAditionalsHeaders = ["Content-Type": contentType.rawValue]
            self.isJson = isJson
            
            for (key, value) in headers {
                self.httpAditionalsHeaders[key] = value
            }
        }
    }
}

extension WebService {
    
    public class Request {
        
        private let urlString: String
        private let method: HTTPMethod
        private let configurtion: Configuration
        private let params: Any?
        
        public var task: URLSessionTask?
        
        private var urlRequest: URLRequest? {
            guard let url = URL(string: self.urlString) else { return nil }
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = self.method.rawValue
            return urlRequest
        }
        
        private var sessionConfiguration: URLSessionConfiguration {
            let sessionConfiguration = URLSessionConfiguration.default
            sessionConfiguration.timeoutIntervalForRequest = self.configurtion.timeout
            sessionConfiguration.httpAdditionalHeaders = self.configurtion.httpAditionalsHeaders
            return sessionConfiguration
        }
        
        private var session: URLSession {
            URLSession(configuration: sessionConfiguration)
        }
        
        private var bodyRequest: Data? {
            guard let params = self.params else { return nil }
            return try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }
        
        public  init(urlString: String, method: HTTPMethod = .get, params: Any? = nil, configuration: Configuration = Configuration()) {
            self.urlString = urlString
            self.method = method
            self.configurtion = configuration
            self.params = params
        }
        
        @discardableResult public func execute(_ completionHandler: @escaping ResponseCompletionHanlder) -> Self {
            guard let urlRequest = self.urlRequest else {
                completionHandler(Response())
                return self
            }
            
            self.method == .get ?
            self.downloadDataTaskWithRequest(urlRequest, completionHandler: completionHandler) :
            self.uploadDataTaskWithRequest(urlRequest, completionHandler: completionHandler)
            
            return self
        }
        
        private func downloadDataTaskWithRequest(_ urlRequest: URLRequest, completionHandler: @escaping ResponseCompletionHanlder) {
            
            self.task = self.session.dataTask(with: urlRequest) { data, urlReponse, error in
                DispatchQueue.main.async {
                    let reponse = Response(data: data, urlResponse: urlReponse, error: error)
                    if self.configurtion.isJson {
                        reponse.printJSON()
                    }
                    completionHandler(reponse)
                }
            }
            
            self.task?.resume()
        }
        
        private func uploadDataTaskWithRequest(_ urlRequest: URLRequest, completionHandler: @escaping ResponseCompletionHanlder) {
            
            self.task = self.session.uploadTask(with: urlRequest, from: self.bodyRequest) { data, urlReponse, error in
                DispatchQueue.main.async {
                    let reponse = Response(data: data, urlResponse: urlReponse, error: error)
                    if self.configurtion.isJson {
                        reponse.printJSON()
                    }
                    completionHandler(reponse)
                }
            }
            
            self.task?.resume()
        }
    }
}

extension WebService {
    
    public struct Response {
        
        private let data: Data?
        private let urlResponse: URLResponse?
        private let error: Error?
        
        public var toImage: UIImage? {
            guard let data = self.data else { return nil }
            
            return UIImage(data: data)
        }
        
        public var urlOrigin: String {
            self.urlResponse?.url?.absoluteString ?? ""
        }
        
        public var hasError: Bool { self.error != nil }
        
        public var errorMessage: String { "ERROR: \(self.error?.localizedDescription ?? "No se encontraron errores")"}
        
        fileprivate init(data: Data? = nil, urlResponse: URLResponse? = nil, error: Error? = nil) {
            self.data = data
            self.urlResponse = urlResponse
            self.error = error
        }
        
        public func toDTO<T>(_ type: T.Type) -> T? where T: Decodable {
            guard let data = self.data else { return nil }
            
            let jsonDecoder = JSONDecoder()
            return try? jsonDecoder.decode(type, from: data)
        }
        
        fileprivate func printJSON() {
            guard let data = self.data else {
                self.printResponse("El servicio no responde")
                return
            }
            let json = try? JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed)
            self.printResponse(json ?? "No se puede leer la trama")
        }
        
        private func printResponse(_ response: Any) {
            print("\n\n***************************************")
            print("Response: \(response)")
            print("\n\n***************************************")
        }
    }
}
