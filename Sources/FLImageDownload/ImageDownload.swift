//
//  ImageDownloader.swift
//  FLCore
//
//  Created by flujan on 29/7/22.
//

import UIKit
import FLWebService

public typealias DownloadImageCompletion = (_ image: UIImage?, _ urlOrigin: String) -> Void

extension String {
    
    public func dowloadImageWithCompletion(_ success: @escaping DownloadImageCompletion) {
        
        let configuration = WebService.Configuration(isJson: false)
        WebService.Request(urlString: self, configuration: configuration).execute { reponse in
            
            DispatchQueue.global(qos: .background).async {
                let image = reponse.toImage
                
                DispatchQueue.main.async {
                    success(image, reponse.urlOrigin)
                }
            }
        }
    }
}

